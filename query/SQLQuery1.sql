--Create database SocialNetWork;
--USE SocialNetWork;
Create Table Registration(
	id int identity(1,1) primary key,
	userName varchar (100),
	email varchar(100),
	password varchar(100),
	phoneNumber varchar(100),
	isActive int,
	isApproved int
)
Create Table Article(
	id int identity(1,1) primary key,
	title varchar (100),
	email varchar(100),
	content varchar(100),
	image varchar(100),
	isActive int,
	isApproved int

)

Create Table News(
	id int identity(1,1) primary key,
	title varchar (100),
	email varchar(100),
	content varchar(100),
	isActive int,
	createdOn datetime
	
)
Create Table Events(
	id int identity(1,1) primary key,
	title varchar (100),
	email varchar(100),
	content varchar(100),
	isActive int,
	createdOn datetime
	
)
Create Table Staff(
	id int identity(1,1) primary key,
	userName varchar (100),
	email varchar(100),
	password varchar(100),
	isActive int,
	
)

Select * from Registration;
Select * from Article;
Select * from News;
Select * from Events;