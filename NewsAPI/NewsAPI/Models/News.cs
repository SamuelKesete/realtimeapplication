﻿namespace NewsAPI.Models
{
    public class News
    {
        public int id { get; set; }
        public string title { get; set; }
        public string email { get; set; }
        public string content { get; set; }
        public int isActive { get; set; }
        public string createdOn { get; set; }
        
    }
}
