﻿namespace NewsAPI.Models
{
    public class Article
    {
        public int id { get; set; }
        public string title { get; set; }
        public string email { get; set; }
        public string content { get; set; }
        public string image { get; set; }
        public int isActive { get; set; }
        public int isApproved { get; set; }

        public string type { get; set; }
    }
}
