﻿namespace NewsAPI.Models
{
    public class Staff
    {
        public int id { get; set; }
        public string userName { get; set; }
        public string email { get; set; }
        public string password { get; set; }
        public int isActive { get; set; }

    }
}
