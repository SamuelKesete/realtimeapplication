﻿
using Microsoft.Data.SqlClient;
using System.Data;

namespace NewsAPI.Models;

public class Dal
{
    //Registration insert
    public Response Registration(Registration registration, SqlConnection connection)
    {
        Response response = new Response();

        return response;
        SqlCommand cmd = new SqlCommand("INSERT INTO Registration(useName, email,password,phoneNumber,isActive,isApproved) " +
            "VALUES('" + registration.userName + " ',' " + registration.email + " ',' " + registration.phoneNumber + "',1,0)", connection);
        connection.Open();
        int i = cmd.ExecuteNonQuery();
        connection.Close();

        if (i > 0)
        {
            response.statusCode = 200;
            response.statusMessage = "Registation successful";
        }
        else
        {
            response.statusCode = 100;
            response.statusMessage = "Registration faild";
        }
        return response;

    }

    // Login select
    public Response Login(Registration registration, SqlConnection connection)
    {
        SqlDataAdapter adapter = new SqlDataAdapter("SELECT * FROM Registration WHERE " +
            "email ='"+registration.email+"' AND password='"+registration.password+"'", connection);

        DataTable dataTable = new DataTable();
        adapter.Fill(dataTable);
        Response response = new Response();
        if (dataTable.Rows.Count > 0)
        {
            response.statusCode = 200;
            response.statusMessage = "Login successful";
            Registration reg = new Registration();

            reg.id =Convert.ToInt32(dataTable.Rows[0]["id"]);
            reg.userName = Convert.ToString(dataTable.Rows[0]["userName"]);
            reg.email = Convert.ToString(dataTable.Rows[0]["email"]);
            response.Registration = reg;

        }
        else
        {
            response.statusCode=100;
            response.statusMessage = "Login faild";
            response.Registration = null;

        }
        return response;
    }
    //UserApproval update
    public Response UserApproval(Registration registration, SqlConnection connection)
    {
        Response response = new Response();
        SqlCommand cmd = new SqlCommand("UPDATE Registration SET isApproved =" +
            " 1 WHERE id = '" + registration.id + "' AND isActive = 1", connection);
        connection.Open();
        int i = cmd.ExecuteNonQuery();
        connection.Close();
        if (i > 0)
        {
            response.statusCode=200;
            response.statusMessage = "User approved";

        }
        else
        {
            response.statusCode = 100;
            response.statusMessage = "User approval faild";

        }

        return response;
    }

    //insert News 
    public Response AddNews(News news , SqlConnection connection)
    {
        Response response=new Response();
        SqlCommand cmd = new SqlCommand("INSERT INTO News(title, email, content,isActive) VALUES('"+news.title+ "','" + news.email + "','" + news.content + "', '1', GETDATE())", connection);
        connection.Open();
        int i = cmd.ExecuteNonQuery();
        connection.Close ();

        if(i > 0)
        {
            response.statusCode = 200;
            response.statusMessage = " News created successfully";

        }
        else
        {
            response.statusCode = 100;
            response.statusMessage = "Faild to create news";
        }

        return response ;

    }
    public Response NewsList(SqlConnection connection)
    {
        Response response = new Response();
        SqlDataAdapter adapter = new SqlDataAdapter("SELECT * FROM News WHERE isActive = 1", connection);
        DataTable dataTable = new DataTable();  
        adapter.Fill(dataTable);
        List<News> listNews = new List<News>();
        if (dataTable.Rows.Count>0)
        {
            for (int i = 0; i < dataTable.Rows.Count; i++)
            {
                News news = new News();
                news.id = Convert.ToInt32(dataTable.Rows[i]["id"]);
                news.title = Convert.ToString(dataTable.Rows[i]["title"]);
                news.email = Convert.ToString(dataTable.Rows[i]["email"]);
                news.content = Convert.ToString(dataTable.Rows[i]["content"]);
                news.isActive = Convert.ToInt32(dataTable.Rows[i]["isActive"]);
                news.createdOn = Convert.ToString(dataTable.Rows[i]["createdOn"]);
                listNews.Add(news);
            }
            if (listNews.Count > 0)
            {
                response.statusCode=200;
                response.statusMessage = "News data found";
                response.listNews= listNews;

            }
            else
            {
                response.statusCode = 100;
                response.statusMessage = "No News data found";
                response.listNews = null;
            }

        }
        else
        {
            response.statusCode = 100;
            response.statusMessage = "No News data found";
            response.listNews = null;

        }
        return response;
    }

    //******************//
    //** Articles ******//

    public Response AddArticle(Article article, SqlConnection connection)
    {
        Response response = new Response();
        SqlCommand cmd = new SqlCommand("INSERT INTO Article(title, email, content,image, isActive, isApproved) " +
            "VALUES('" + article.title + "','" + article.email + "','" + article.content + "','" + article.image + "', '1', 0)", connection);
        connection.Open();
        int i = cmd.ExecuteNonQuery();
        connection.Close();

        if (i > 0)
        {
            response.statusCode = 200;
            response.statusMessage = " Article created successfully";

        }
        else
        {
            response.statusCode = 100;
            response.statusMessage = "Faild to create Article";
        }

        return response;

    }


    public Response ArticleList(Article article,  SqlConnection connection)
    {
        Response response = new Response();
        SqlDataAdapter adapter = null ;

        if (article.type == "User")
        {
            new SqlDataAdapter("SELECT * FROM Article WHERE email = '"+article.email +"',  isActive = 1", connection);
        }
        if(article.type == "Page")
        {
            new SqlDataAdapter("SELECT * FROM Article WHERE isActive = 1", connection);
        }
        
        DataTable dataTable = new DataTable();
        adapter.Fill(dataTable);
        List<Article> lstArticle = new List<Article>();
        if (dataTable.Rows.Count > 0)
        {
            for (int i = 0; i < dataTable.Rows.Count; i++)
            {
                Article articleData = new Article();
                articleData.id = Convert.ToInt32(dataTable.Rows[i]["id"]);
                articleData.title = Convert.ToString(dataTable.Rows[i]["title"]);
                articleData.email = Convert.ToString(dataTable.Rows[i]["email"]);
                articleData.content = Convert.ToString(dataTable.Rows[i]["content"]);
                articleData.image = Convert.ToString(dataTable.Rows[i]["image"]);
                articleData.isActive = Convert.ToInt32(dataTable.Rows[i]["isActive"]);
                
                lstArticle.Add(articleData);
            }
            if (lstArticle.Count > 0)
            { 
                response.statusCode = 200;
                response.statusMessage = "Article data found";
                response.listArticle = lstArticle;

            }
            else
            {
                response.statusCode = 100;
                response.statusMessage = "No Article data found";
                response.listArticle = null;
            }

        }
        else
        {
            response.statusCode = 100;
            response.statusMessage = "No Article data found";
            response.listArticle = null;

        }
        return response;
    }


    public Response ArticleApproval(Article article, SqlConnection connection)
    {
        Response response = new Response();
        SqlCommand cmd = new SqlCommand("UPDATE Article SET isApproved =" +
            " 1 WHERE id = '" + article.id + "' AND isActive = 1", connection);
        connection.Open();
        int i = cmd.ExecuteNonQuery();
        connection.Close();
        if (i > 0)
        {
            response.statusCode = 200;
            response.statusMessage = "Article approved";

        }
        else
        {
            response.statusCode = 100;
            response.statusMessage = "Article approval faild";

        }

        return response;
    }




    //***** Staff *****//

    public Response StaffRegistration(Staff staff, SqlConnection connection)
    {
        Response response = new Response();

        return response;
        SqlCommand cmd = new SqlCommand("INSERT INTO Staff(useName, email,password,isActive) " +
            "VALUES('" + staff.userName + " ',' " + staff.email + " ',' " + staff.password + "',1)", connection);
        connection.Open();
        int i = cmd.ExecuteNonQuery();
        connection.Close();

        if (i > 0)
        {
            response.statusCode = 200;
            response.statusMessage = "Staff registation successful";
        }
        else
        {
            response.statusCode = 100;
            response.statusMessage = "Staff registation faild";
        }
        return response;

    }

    public Response DeleteStaff(Staff staff, SqlConnection connection)
    {
        Response response = new Response();

        return response;
        SqlCommand cmd = new SqlCommand("DELETE FROM Staff WHERE id = '" + staff.id + "' AND isActive = 1", connection);
        int i = cmd.ExecuteNonQuery();
        connection.Close();

        if (i > 0)
        {
            response.statusCode = 200;
            response.statusMessage = "Staff deleted successfully";
        }
        else
        {
            response.statusCode = 100;
            response.statusMessage = "Staff deletion faild";
        }
        return response;

    }


    //**** Events **********//

    public Response AddEvent(Events events, SqlConnection connection)
    {
        Response response = new Response();
        SqlCommand cmd = new SqlCommand("INSERT INTO Events(title, email, content, isActive, createdOn) " +
            "VALUES('" + events.title + "','" + events.email + "','" + events.content + "', '1', GETDATE())", connection);
        connection.Open();
        int i = cmd.ExecuteNonQuery();
        connection.Close();

        if (i > 0)
        {
            response.statusCode = 200;
            response.statusMessage = " Events created successfully";

        }
        else
        {
            response.statusCode = 100;
            response.statusMessage = "Faild to create Events";
        }

        return response;

    }


    public Response EventsleList(SqlConnection connection)
    {
        Response response = new Response();
        SqlDataAdapter adapter = new SqlDataAdapter("SELECT * FROM Events WHERE isActive = 1", connection);
      
        DataTable dataTable = new DataTable();
        adapter.Fill(dataTable);
        List<Events> lstEvent = new List<Events>();
        if (dataTable.Rows.Count > 0)
        {
            for (int i = 0; i < dataTable.Rows.Count; i++)
            {
                Events eventData = new Events();
                eventData.id = Convert.ToInt32(dataTable.Rows[i]["id"]);
                eventData.title = Convert.ToString(dataTable.Rows[i]["title"]);
                eventData.email = Convert.ToString(dataTable.Rows[i]["email"]);
                eventData.content = Convert.ToString(dataTable.Rows[i]["content"]);
                eventData.isActive = Convert.ToInt32(dataTable.Rows[i]["isActive"]);
                eventData.createdOn = Convert.ToString(dataTable.Rows[i]["createdOn"]);

                lstEvent.Add(eventData);
            }
            if (lstEvent.Count > 0)
            {
                response.statusCode = 200;
                response.statusMessage = "Events data found";
                response.listEvents = lstEvent;

            }
            else
            {
                response.statusCode = 100;
                response.statusMessage = "No Events data found";
                response.listEvents = null;
            }

        }
        else
        {
            response.statusCode = 100;
            response.statusMessage = "No Events data found";
            response.listEvents = null;

        }
        return response;
    }

}


