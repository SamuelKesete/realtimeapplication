﻿namespace NewsAPI.Models
{
    public class Registration
    {
        public int id { get; set; }
        public string userName { get; set; }
        public string email { get; set; }
        public string password { get; set; }
        public string phoneNumber { get; set; }
        public int isActive { get; set; }
        public int isApproved { get; set; }

    }
}
