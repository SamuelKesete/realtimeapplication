﻿namespace NewsAPI.Models
{
    public class Response
    {
        public int statusCode { get; set; }
        public string statusMessage { get; set; }
        public List<Registration> listRegistrations { get; set; }
        public Registration Registration { get; set; }
        public List<Article> listArticle{ get; set; }
        public List<News> listNews { get; set; }
        public List<Events> listEvents { get; set; }
    }
}
