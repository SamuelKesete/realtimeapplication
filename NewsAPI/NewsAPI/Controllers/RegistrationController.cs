﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using NewsAPI.Models;

namespace NewsAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RegistrationController : ControllerBase
    {

        //create un object of IConfiguration class 
        private readonly IConfiguration _configuration;

        public RegistrationController(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        // include sql client 

        [HttpPost]
        [Route("Registration")]

        public Response Registration(Registration registration)
        {
            Response response = new Response();
            SqlConnection connection = new SqlConnection(_configuration.GetConnectionString("SNcon").ToString());
            Dal dal = new Dal();
             response = dal.Registration(registration, connection);

            return response;
        }

        [HttpPost]
        [Route("Login")]
        public Response Login(Registration registration)
        {
            Response response = new Response();
            SqlConnection connection = new SqlConnection(_configuration.GetConnectionString("").ToString());

            Dal dal = new Dal();
            response = dal.Login(registration, connection);

            return response;
        }

        [HttpPost]
        [Route("UserApproval")]

        public Response UserApproval(Registration registration)
        {
            Response response = new Response();
            SqlConnection connection = new SqlConnection(_configuration.GetConnectionString("").ToString());
            Dal dal = new Dal();    
            response = dal.UserApproval(registration, connection);

            return response;

        }



        [HttpPost]
        [Route("StaffRegistration")]

        public Response StaffRegistration(Staff staff)
        {
            Response response = new Response();
            SqlConnection connection = new SqlConnection(_configuration.GetConnectionString("SNcon").ToString());
            Dal dal = new Dal();
            response = dal.StaffRegistration(staff, connection);

            return response;
        }



        [HttpPost]
        [Route("DeleteStaff")]

        public Response DeleteStaff(Staff staff)
        {
            Response response = new Response();
            SqlConnection connection = new SqlConnection(_configuration.GetConnectionString("SNcon").ToString());
            Dal dal = new Dal();
            response = dal.DeleteStaff(staff, connection);

            return response;
        }
    }
}
